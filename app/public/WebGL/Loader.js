if (!(!Math.fround)) {
var script = document.createElement('script');
script.src = "/WebGL/Release/WebGL.js";
document.body.appendChild(script);
} else {
var codeXHR = new XMLHttpRequest();
codeXHR.open('GET', '/WebGL/Release/WebGL.js', true);
codeXHR.onload = function() {
var code = codeXHR.responseText;
if (!Math.fround) {
	console.log('optimizing out Math.fround calls');
	code = code.replace(/Math_fround\\\(/g, '(').replace("'use asm'", "'almost asm'")
}
var blob = new Blob([code], { type: 'text/javascript' });
codeXHR = null;
var src = URL.createObjectURL(blob);
var script = document.createElement('script');
script.src = URL.createObjectURL(blob);
script.onload = function() {
URL.revokeObjectURL(script.src);
};
document.body.appendChild(script);
};
codeXHR.send(null);
}
